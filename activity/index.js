/*
	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
    2. Create a class constructor able to receive 3 arguments
        It should be able to receive 3 strings and a number
        Using the this keyword assign properties:
        username, 
        role, 
        guildName,
        level 
  assign the parameters as values to each property.
  Create 2 new objects using our class constructor.
  This constructor should be able to create Character objects.


	Pushing Instructions:

	Create a git repository named S24.
	Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	Add the link in Boodle.

*/

//Solution: 

/*Debug*/
let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101","Social Sciences 201"]
}


let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}


/*Debug and Update*/

function introduce(student){

	console.log(`Hi! I'm  ${student.name} I am ${student.age} years old.`)
	console.log(`I study the following courses: ${student.classes}`)

}

introduce(student1);
introduce(student2);

// Traditional 1
// function getCube(num){
// 	console.log(Math.pow(num,3));
// }


// Arrow Func 1
const getCube = (num) => num * 3;
console.log(getCube(3));



// Traditional 2
// let numArr = [15,16,32,21,21,2]
// numArr.forEach(function(num){
// 	console.log(num);
// })


// Arrow 2
let numArr = [15,16,32,21,21,2];

numArr.forEach(num => {
	console.log(num); 
})



// let numsSquared = numArr.map(function(num){

// 	return number ** 2;

//   }
// )

let numsSquared = numArr.map(num => {
	return num**2
	console.log(numsSquared);
})

console.log(numsSquared);

/*2. Class Constructor*/
// Create a class constructor able to receive 3 arguments
//         It should be able to receive 3 strings and a number
//         Using the this keyword assign properties:
//         username, 
//         role, 
//         guildName,
//         level 
//   assign the parameters as values to each property.
//   Create 2 new objects using our class constructor.
//   This constructor should be able to create Character objects.

class Character {
	constructor(username,role,guildName,level){
		this.username = username;
		this.role = role;
		this.guildName = guildName;
		this.level = level;
	}
}

let character1 = new Character("Lord Grim","Swordsman","ExcellentOne",99);
let character2 = new Character("kinon","Mage","BlueLeaf",99);

console.log(character1);
console.log(character2);