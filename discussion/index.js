console.log("Hello World");

// ES6 Updates
// ES6 is one of the latest versions of writing Javascript and one of the latest major updates in JS.

/* 
	Creating Variables
	1. let - 
	2. const - 
	3. var - was the keyword to create variables before ES6
	>> Hoisting is the danger on using the var
*/

// console.log(varSample);
// var varSample = "Hoist me up!";

//Exponent Operator

let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3);
// Math.pow() allows us to get the result of a number raised to a given exponent

/*
	Syntax:
		Math.pow(base,exponent);
*/

// Exponent operator ** alows us to get the result of a number raised to a given exponent. Alternative to Math.pow();
let exponentOperator = 5**2;
console.log(exponentOperator);

let fivePowerOf4 = 5**4;
console.log(fivePowerOf4);

let string1 = "Javascript";
let string2 = "not";
let string3 = "is";
let string4 = "Typescript";
let string5 = "Java";
let string6 = "Zuitt";
let string7 = "Coding";


// Create a new variable called sentence1 and sentence2

/*
let sentence1 = string1+" "+string3+" "+string2+" "+string5;
let sentence2 = string4+" "+string3+" "+string1;

console.log(sentence1);
console.log(sentence2);
*/

// Template Literals
// "",'' - string literals
// Template allows us to create use `` and easily embed JS expression with it

let sentence1 = `${string1} ${string3} ${string2} ${string5}`;

let sentence2 = `${string4} ${string3} ${string1}`;

console.log(sentence1);
console.log(sentence2);

// $() is a placeholder it is used to embed JS expression when creating strings using Template Literals

let sentence3 = `${string6} ${string7} ${"Bootcamp"}`;
console.log(sentence3);

let person = {
	name: "Michael",
	position: "developer",
	income: 50000,
	expense: 60000
}

console.log(`${person.name} is a ${person.position}`);

// Destructuring Arrays and Objects
// Destructuring will allow us to save Array items or Object properties into new variables without having to create/initialize with accessing the items/properties one by one

let array1 = ["Curry","Lillard","Paul","Irving"];

// let player1 = array1[0];
// let player2 = array1[1];
// let player3 = array1[2];
// let player4 = array1[3];

// console.log(player1,player2,player3,player4);

// Array desctructuring is when we save array items into variables.
// In arrays, order matters, and that goes the same to our destructuring

let [player1,player2,player3,player4] = array1
console.log(player1,player2,player3,player4);

let array2 = ["Jokic","Embiid","Howard","Anthony-Towns"];
// Get and save all items into variables except for Howard

let[center1,center2,,center4] = array2;

console.log(center4);

// Object Destructuring
// In object destructuring, the order of destructuring does not matter, however, the name of the variable must match a property in the object.

let pokemon1 = {

	name: "Bulbasaur",
	type: "Grass",
	level: 10,
	moves: ["Razor Leaf","Tackle","Leech Seed"]

}

let {level,type,name,moves,personality} = pokemon1;

console.log(level);
console.log(type);
console.log(name);
console.log(moves);

let pokemon2 = {

	name: "Charmander",
	type: "Fire",
	level: 10,
	moves: ["Ember","Scratch"]

}

// let {moves1,type1,name1,level1} = pokemon2
// console.log(moves1);

const {name: name2} = pokemon2;
console.log(name2);


// Arrow Functions
// Arrow functions are alternative ways of writing functions in JS. However, there are significant pros and cons between traditional and arrow functions

// Traditional Function

function displayMsg(){
	console.log("Hello, World!");
}

displayMsg();

const hello = () => {
	console.log("Hello from Arrow!");
}

hello();

// Arrow Function with Parameters
const greet = (friend) => {
	console.log(`Hi! ${friend.name}`);
}

greet(person);


// Arrow vs Traditional Function

// Implicit Return - allows us to return a value from an arrow function without the use of a return keyword


// function addNum(num1,num2){

// 	// console.log(num1,num2);
// 	return num1+num2;
// }

// let sum = addNum(5,10)
// console.log(sum);

// let subNum = (num1,num2) => num1 + num2;
// let difference = subNum(10,5);
// console.log(difference);

// Implicit return will only work on arrow functions without {}
// {} in arrow functions/functions are code block. If an arrow function has a {} or code block, we're going to need to use a return keyword

// translate our previous addNum traditional function into an arrow function

const addNum = (num1,num2) => num1+num2;

let sum = addNum(50,70)
console.log(sum);

// Traditional Function vs Arrow Function as Methods

let character1 = {

	name: "Cloud Strife",
	occupation: "SOLDIER",
	greet: function(){
		
		// in a traditional function as a method:
		// this keyword refers to the current object where the method is.
		console.log(this);
		console.log(`Hi! I'm ${this.name}`);
	},
	introduceJob: () => {
		// In an arrow function as method
		// The this keyword WILL NOT reger to the current object. Instead, it will refer to the global window object.
		console.log(this);
	}
}

character1.greet();
character1.introduceJob();

// Class Based Objects Blueprints
	// in Javascript, classes are temples of objects.
	// We can create objects out of the use of classes.
	// Before the introduction of classes in JS, we mimic this behavior of being able to create objects of the same blueprint using the constructor function.

	// Constructor Function
	// function Pokemon(name,type,level){
	// 	this.name = name;
	// 	this.type = type;
	// 	this.level = level;
	// }

	// With the advent of ES6, we are now introduced to a special method of creating and initializing an object

	class Car {
		constructor(brand,name,year){

			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}

	let car1 = new Car("Toyota","Vios","2002");
	let car2 = new Car("Cooper","Mini","1969");
	let car3 = new Car("Porsche","911","1967");

	console.log(car1);
	console.log(car2);
	console.log(car3);

class Pokemon {
	constructor(name,type,level){
		this.name = name;
		this.type = type;
		this.level = level;
	}
}

	let pokeball1 = new Pokemon("Vulpix","Fire",65);
	let pokeball2 = new Pokemon("Onyx","Rock",53);
	let pokeball3 = new Pokemon("Ponyta","Fairy",44);

	console.log(pokeball1);
	console.log(pokeball2);
	console.log(pokeball3);